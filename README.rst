This fork
===

This repository is a fork from https://github.com/stapelberg/pretix-datatrans (Apache 2.0).
NB: the plugin uses the legacy "v1" Datatrans API

It includes the following modifications :
- enable other Datatrans payment methods
  - TWINT (TWI)
  - Visa (VIS)
  - Mastercard (ECA)
  - PayPal (PAP)
  - PostFinance Card (PFC)
  - PostFinance E-Finance (PEF)
- payment reference prefix customization in event settings

Useful links
- https://docs.datatrans.ch/v1.0.1/docs/credit-cards
- https://docs.datatrans.ch/v1.0.1/docs/other-payment-methods

Datatrans
==========================

This is a plugin for `pretix`_. 

datatrans payment provider integration

Development setup
-----------------

1. Make sure that you have a working `pretix development setup`_.

2. Clone this repository.

3. Activate the virtual environment you use for pretix development.

4. Execute ``python setup.py develop`` within this directory to register this application with pretix's plugin registry.

5. Execute ``make`` within this directory to compile translations.

6. Restart your local pretix server. You can now use the plugin from this repository for your events by enabling it in
   the 'plugins' tab in the settings.

This plugin has CI set up to enforce a few code style rules. To check locally, you need these packages installed::

    pip install flake8 isort black

To check your plugin for rule violations, run::

    black --check .
    isort -c .
    flake8 .

You can auto-fix some of these issues by running::

    isort .
    black .

To automatically check for these issues before you commit, you can run ``.install-hooks``.


License
-------


Copyright 2023 Michael Stapelberg

Released under the terms of the Apache License 2.0



.. _pretix: https://github.com/pretix/pretix
.. _pretix development setup: https://docs.pretix.eu/en/latest/development/setup.html
